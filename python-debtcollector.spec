%global __requires_exclude %{python3_version}dist.*wrapt.*
%global _empty_manifest_terminate_build 0
Name:           python-debtcollector
Version:        3.0.0
Release:        1
Summary:        A collection of Python deprecation patterns and strategies that help you collect your technical debt in a non-destructive manner.
License:        Apache-2.0
URL:            https://docs.openstack.org/debtcollector/latest
Source0:        https://files.pythonhosted.org/packages/c8/7d/904f64535d04f754c20a02a296de0bf3fb02be8ff5274155e41c89ae211a/debtcollector-%{version}.tar.gz
BuildArch:      noarch
%description
A collection of Python deprecation patterns and strategies that help you collect your technical debt in a non-destructive manner.

%package -n python3-debtcollector
Summary:        A collection of Python deprecation patterns and strategies that help you collect your technical debt in a non-destructive manner.
Provides:       python-debtcollector
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-wrapt
# General requires
Requires:       python3-wrapt
%description -n python3-debtcollector
A collection of Python deprecation patterns and strategies that help you collect your technical debt in a non-destructive manner.

%package help
Summary:        A collection of Python deprecation patterns and strategies that help you collect your technical debt in a non-destructive manner.
Provides:       python3-debtcollector-doc
%description help
A collection of Python deprecation patterns and strategies that help you collect your technical debt in a non-destructive manner.

%prep
%autosetup -n debtcollector-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .


%files -n python3-debtcollector -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Mar 26 2024 wangqiang <wangqiang1@kylinos.cn> - 3.0.0-1
- Update package to version 3.0.0

* Tue May 10 2022 OpenStack_SIG <openstack@openeuler.org> - 2.5.0-1
- Upgrade package python3-debtcollector to 2.5.0 to support OpenStack Yoga

* Sat Jan 30 2021 zhangy <zhangy1317@foxmail.com>
- Add buildrequires

* Fri Nov 20 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
